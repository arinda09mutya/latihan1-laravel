<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/daftar" method="POST">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="firstname"><br>
        <br>
        <label>Last name:</label><br>
        <input type="text" name="lastname"><br>
        <br>
        <label>Gender:</label><br>
            <input type="radio" id="male" name="gender"> Male <br>
            <input type="radio" id="female" name="gender"> Female <br>
            <input type="radio" id="other" name="gender"> Other
        <br>
        <br>
        <label>Nationality:</label><br>
        <br>
            <select name="nationality" id="">
                <option value="">Indonesia</option>
                <option value="">Korea Selatan</option>
            </select>
        <br>
        <br>
        <label>Languange Spoken:</label><br>
            <input type="checkbox"> Bahasa Indonesia <br>
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other <br>
        <br>
        <label>Bio:</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br>
        <button>Sign Up</button>
    </form>
</body>
</html>