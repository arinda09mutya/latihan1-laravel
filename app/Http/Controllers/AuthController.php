<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('page.form'); 
        // ngambil blade nya
    }
    public function submit(Request $request){
        $namaawal = $request['firstname'];
        $namaakhir = $request['lastname'];
        return view('page.selamat', compact('namaawal', 'namaakhir'));
    }
}
